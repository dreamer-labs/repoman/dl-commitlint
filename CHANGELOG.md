## [1.0.4](https://gitlab.com/dreamer-labs/repoman/dl-commitlint/compare/v1.0.3...v1.0.4) (2020-10-21)


### Bug Fixes

* Add support for timeouts and port handling ([af70dac](https://gitlab.com/dreamer-labs/repoman/dl-commitlint/commit/af70dac))

## [1.0.3](https://gitlab.com/dreamer-labs/repoman/dl-commitlint/compare/v1.0.2...v1.0.3) (2020-10-21)


### Bug Fixes

* Add bugfix for better secrets handling ([c0b6c7a](https://gitlab.com/dreamer-labs/repoman/dl-commitlint/commit/c0b6c7a))

## [1.0.2](https://gitlab.com/dreamer-labs/repoman/dl-commitlint/compare/v1.0.1...v1.0.2) (2020-10-21)


### Bug Fixes

* Add functional evaluation testing ([3b58e62](https://gitlab.com/dreamer-labs/repoman/dl-commitlint/commit/3b58e62))

## [1.0.1](https://gitlab.com/dreamer-labs/repoman/dl-commitlint/compare/v1.0.0...v1.0.1) (2020-10-20)


### Bug Fixes

* Fix bug with URL rewriting ([05bc44b](https://gitlab.com/dreamer-labs/repoman/dl-commitlint/commit/05bc44b))

# 1.0.0 (2019-11-26)


### Bug Fixes

* Correct testing issues ([fadfe39](https://gitlab.com/dreamer-labs/repoman/dl-commitlint/commit/fadfe39))


### Features

* Add commitlintit wrapper script ([d904ca1](https://gitlab.com/dreamer-labs/repoman/dl-commitlint/commit/d904ca1))
