# test_001

In this test, the `UPSTREAM_URL` set to an ssh git URL, but no ssh key is on the system, so it falls back to the https url and is ultimately able to get it to pass as a result.

Addtionally, `COMMITLINT_CONFIG` is unspecified, so it falls back to the implicit, and then eventually the default file pathes. As a result, `commmitlint` ultimately passes.
