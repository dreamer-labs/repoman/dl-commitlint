# test_int_007

The `UPSTREAM_URL` is completely wrong (i.e. not an open port and an invalid url). `commitlintit` tries to fix this, but fails to. It cycles the `UPSTREAM_URL` through https, http, and ssh, before finally giving up after the appropriate number of tries, and then suffers a fatal error, passing the user a '101' return code. The return code and last setting of `UPSTREAM_URL` are checked just before exit to validate the correct behavior. If the port number is not properly stripped with the url parser function, this test returns '61'.
