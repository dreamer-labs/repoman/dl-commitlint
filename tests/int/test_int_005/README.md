# test_005

The `UPSTREAM_URL` is completely wrong (i.e. non-existent repo). `commitlintit` tries to fix this, but fails to. It cycles the `UPSTREAM_URL` through https, http, and ssh, before finally giving up after the appropriate number of tries, and then suffers a fatal error, passing the user a '101' return code. The return code and last setting of `UPSTREAM_URL` are checked just before exit to validate the correct behavior.
