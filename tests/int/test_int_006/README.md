# test_int_006

The `UPSTREAM_URL` is completely wrong (i.e. non-existent repo). `commitlintit` tries to fix this, but fails to. It cycles the `UPSTREAM_URL` through https, http, and ssh, before finally giving up after the appropriate number of tries, and then suffers a fatal error, passing the user a '101' return code. The return code and last setting of `UPSTREAM_URL` are checked just before exit to validate the correct behavior.

This test is different from `test_int_005` in one important aspect: the url it originally passes container a username:password string in front of the hostname. This should be stripped off and not passed to http and ssh. the final url should not contain it.
