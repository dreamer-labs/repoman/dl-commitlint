# fe test_002

The main purpose of this test is to ensure that `commitlintit` can gracefully recover when a user attempts to pass an invalid `UPSTREAM_URL` to it. Specifically, an invalid URL that has a valid hostname (and repo), but an invalid port number.

A bug was previously discovered that resulted in the script to hang indefinitely when it encounters this situation, so a timeout feature was added to place a configurable timeout, (with a reasonable default), on all `git fetch` requests made to git repositories. This test should bypass the http URL originally passed to it after that one fails, and then should pass when attempting the https version of the same URL (without the port number). By proxy, this test tests the same thing that `test_int_007`tests: the ability of the script to parse and remove port numbers from the end of hostnames.
