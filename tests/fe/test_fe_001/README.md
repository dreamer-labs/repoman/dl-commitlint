# fe test_001

This test is basically a smoke test that makes sure that commitlintit still runs when the `COWABUNGA_CONFIG` variable isn't set. This has caused failures previously.
